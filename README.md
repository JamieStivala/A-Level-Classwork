# The use of this Git
The use of this git is to upload all the homework and classwork done.
This will keep everything organised and nothing is lost.


# File Structure
Each lesson has it's own package. Each folder will have a minimum of two
other packages and for every program with more then one class in them
will have there own package; as shown:

    ├── Classwork
    |   ├── Some OOP Based Programs Packages
    ├── Homework
    |   ├── Some OOP Based Programs Packages
