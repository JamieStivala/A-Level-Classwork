package Lesson7.Classwork;


public class Deliveryman extends HourlyRateEmployee {
    
    private float overTimeRate;
    
    public Deliveryman () {
        setOverTimeRate (1);
    }
    
    public Deliveryman (String i, String n, int h, float nr, float or) {
        super(i, n, h, nr);
        setOverTimeRate (or);
    }
    
    public float getOverTimeRate () {
        return overTimeRate;
    }
    
    public void setOverTimeRate (float hr) {
        if (hr >= 1) {
            overTimeRate = hr;
        }
    }
    
    @Override
    public float getSalary () {
        if (getHours() > 40) {
            return (40 * getHourlyRate()) + (getHours() - 40) * overTimeRate; 
        } else {
            return getHours() * getHourlyRate();
        }
    }
    
}
