package Lesson7.Classwork;


public  abstract class FlatRateEmployee extends Employee {
    private float basicPay;
    
    public FlatRateEmployee () {
        setBasicPay(1);
    }
    
    public FlatRateEmployee(String i, String n, float bp) {
        super (i, n);
        setBasicPay(bp);
    }
    
    public void setBasicPay (float bp) { 
        if (bp >= 1) {
            basicPay = bp;
        }
    }
    
    public float getBasicPay () {
        return basicPay;
    }
    
}
