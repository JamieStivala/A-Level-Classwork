package Lesson7.Classwork;

import java.util.*;
public class Exception1 {
    public static void main (String args[]){
        Scanner scanner = new Scanner (System.in);

        try{
            System.out.println("Enter a number");
            int x = scanner.nextInt();
            int y = 56/0;
            System.out.println(y);
        }catch (InputMismatchException e){
            System.out.println("Invalid type String");
        }catch (ArithmeticException e){
            System.out.println("Can't divide by 0");
        }
    }
}
