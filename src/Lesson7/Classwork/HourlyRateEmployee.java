package Lesson7.Classwork;

public abstract class HourlyRateEmployee extends Employee {
    private int hours;
    private float hourlyRate;
    
    public HourlyRateEmployee () {
        hours = 0;
    }
    
    public HourlyRateEmployee (String i, String n, int h, float hr) {
        super(i, n);
        setHours(h);
        setHourlyRate(hr);
    }
    
    public float getHourlyRate () {
        return hourlyRate;
    }
    
    public void setHourlyRate (float hr) {
        if (hr >= 1) {
            hourlyRate = hr;
        }
    }
    
    public int getHours () {
        return hours;
    }
    
    public void setHours (int h) {
        if (h >= 0) {
            hours = h;
        }
    }

}
