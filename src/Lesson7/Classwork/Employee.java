package Lesson7.Classwork;


public abstract class Employee {
    
    private String ID;
    private String name;
    
    public Employee () {
    }
    
    public Employee (String i, String n) {
        setID (i);
        setName (n);
    }
    
    public String getID () {
        return ID;
    }
    
    public void setID (String i) {
        ID = i;
    }
    
    public String getName () {
        return name;
    }
    
    public void setName (String n) {
        name = n;
    }
    
    public abstract float getSalary ();
    
    public int compareTo(Object o){
        return this.getName().compareTo(((Employee) o).getName());
    }    
}
