package Lesson7.Classwork;


import java.awt.*;
public final class Manager extends FlatRateEmployee implements Drawable{
    
    public Manager () {
    }
    
    public Manager (String i, String n, float bp) {
        super (i, n, bp);
    }
    
    @Override
    public float getSalary () {
        return getBasicPay();
    }
    
    public void draw(Graphics g){
        g.drawString (getName(), 10, 10);
    }
}
