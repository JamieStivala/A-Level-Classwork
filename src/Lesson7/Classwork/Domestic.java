package Lesson7.Classwork;

public class Domestic extends HourlyRateEmployee {
    
    public Domestic () {
    }
    
    public Domestic (String i, String n, int h, float hr) {
        super(i, n, h, hr);
    }
    
    @Override
    public float getSalary () {
        return getHours() * getHourlyRate();
    }
}
