package Lesson7.Classwork;

public class Payroll {
    public static void main (String args []){
        Employee e[] = new Employee[5];
        
        e[0] = new Manager ("2222", "Joey", 1000);
        e[1] = new Salesman ("4532389", "Mark", 600, 4000);
        e[2] = new Domestic ("7856", "Michela", 20, 5.66f);
        e[3] = new Salesman ("4532389", "Emanuel", 200, 4000);
        e[4] = new Salesman ("4532389", "Chris", 900, 3000);
        
        
        Object o = new Salesman();
        
        for(Employee emp: e){
            System.out.println (emp.getSalary());
            if (emp instanceof Domestic){
                System.out.println(((Domestic)emp).getHourlyRate());
            }
        }
        
        Manager m = new Manager ("12346", "Martin Vella", 1000);
        Salesman  s = new Salesman ("23432", "Mark Farrugia", 600, 4000);
    }
}
