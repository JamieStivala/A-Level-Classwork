package Lesson7.Classwork;


import java.util.*;
public class PayrollArrayList{
    public static void main (String args []){
        ArrayList<Employee> e = new ArrayList <Employee> ();
        
        //Diffrent Syntax - Adding an object:
        e.add (new Manager ("12342", "Mark", 3000));
        
        Domestic d = new Domestic("8765", "Christopher", 100, 1000);
        e.add(d);
        //Diffrent Syntax - Adding an object
        
        //Accessing An Item
        e.get (0);
        if(e.get (0) instanceof Manager){
            //Do something ....
        }
        //Accessing An Item
        
        e.remove (1); //Removing items
        
        e.size(); //Get the size of ArrayList
        
        //Diffrent Syntax - Traversing the list
        for (Employee emp: e){
            System.out.println(emp.getName());
        }
        
        for(int i = 0; i < e.size(); i++){
            System.out.println(e.get(i).getName());
            
            if(e.get(i) instanceof Manager){
                System.out.println (((Manager) (e.get(i))).getBasicPay());
            }
        }
        //Diffrent Syntax - Traversing the list
        
    }
}
