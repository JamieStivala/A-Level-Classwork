package Lesson7.Classwork;


public class Salesman extends FlatRateEmployee {
    private float sales;
    
    public Salesman () {
        setSales(0);
    }
    
    public Salesman (String i, String n, float r, float s) {
        super (i, n, r);
        setSales(s);
    }
    
    public void setSales (float s) { 
        if (s >= 0) {
            sales = s;
        }
    }
    
    public float getSales () {
        return sales;
    }
    
    @Override
    public float getSalary () {
        return (sales * 0.05f) + getBasicPay();
    }
    
}
