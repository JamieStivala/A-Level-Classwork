package Lesson7.Homework;

import java.util.Scanner;
import Tools.FileTasks;
public class SchoolMarks {
    public static void main (String args []){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the subject");
        String subject = scanner.nextLine();

        System.out.println("Please enter how many students there are in class");
        int student = scanner.nextInt();

        FileTasks fileTasks = new FileTasks(subject + ".txt", true);

        for(int i = 0; i != student; i++){
            System.out.println("Please enter the name of the student");
            String name = scanner.nextLine();
            System.out.println("Please enter the surname of the student");
            String surname = scanner.nextLine();
            System.out.println("Please enter the mark the user got");
            int marks = scanner.nextInt();

            fileTasks.addLine(name);
            fileTasks.addLine(surname);
            fileTasks.addLine(marks + "");

        }
        fileTasks.loadToFile();
        fileTasks.close();

    }
}
