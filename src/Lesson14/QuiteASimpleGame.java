package Lesson14;

import java.awt.*;
import java.awt.event.*;
import javax.imageio.*;
import java.awt.image.*;
import java.io.*;

public class QuiteASimpleGame extends Frame implements KeyListener{
    private BufferedImage background;
    private UpdateFrame animationThread;
    private Sprite apple, basket;

    QuiteASimpleGame(String title){
        super(title);
        this.setSize(640, 480);
        apple = new Sprite ("apple.png", 320, 20, 50, 50);
        basket = new Sprite ("basket.png", 320, 430, 50, 50);
        addWindowListener (new WindowAdapter(){
                public void windowClosing (WindowEvent w){
                    System.exit(0);
                    animationThread.setStopped(true);
                }
            });
            
        try {
            background = ImageIO.read(new File("src/Lesson14/background.jpg"));
        }catch(Exception e){
            System.out.println("Image not found");
        }

        this.addKeyListener(this);
        this.requestFocus();
        
        animationThread = new UpdateFrame(this);
        animationThread.start();
    }

    @Override
    public void paint(Graphics g){
        g.drawImage (background, 1, 1, null);
        apple.paint(g);
        basket.paint(g);
    }

    @Override
    public void update(Graphics g){
        paint(g);
    }

    public void keyPressed (KeyEvent e){
        if(e.getKeyCode() == KeyEvent.VK_A){
            basket.moveLeft(1);
        }

        if(e.getKeyCode() == KeyEvent.VK_D){
            basket.moveRight(1);
        }
    }

    public void keyReleased (KeyEvent e) {}

    public void keyTyped (KeyEvent e) {}
    
    
    Sprite getApple(){
        return this.apple;
    }
    
}
