package Lesson14;

public class UpdateFrame extends Thread{
    private QuiteASimpleGame frame;
    private boolean stopped = false;
    
    UpdateFrame(QuiteASimpleGame frame){
        super();
        this.frame = frame;
    }
    
    @Override
    public void run(){
        while (!stopped){
            frame.getApple().moveDown(3);
            frame.repaint();
            try{
                Thread.sleep(100);
            }catch(InterruptedException x){
                x.printStackTrace();
            }
        }
    }
    
    void setStopped(boolean stopped){
        this.stopped = stopped;
    }
}
