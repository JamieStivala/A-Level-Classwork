package Lesson5;


public class BankAccount{
    static float intrestRate;
    String number;
    float balance;
    Client owner;
    
    static {
        intrestRate = 6;
    }
    
    public BankAccount (){
        this.balance = 0;
    }
    
    public BankAccount (float balance){
        this.balance = balance;
    }
    
    void depositBalance(float amount){
        balance += amount;
    }
    
    void withdrawBalance(float amount){
        balance -= amount;
    }
    
    boolean isOverdraft(){
        return balance < 0;
    }
    
    boolean hasFunds(float amount){
        return balance >= amount;
    }
}
