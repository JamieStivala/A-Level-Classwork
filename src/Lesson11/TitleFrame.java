package Lesson11;
import java.awt.*;
import java.awt.event.*;

public class TitleFrame extends Frame implements ActionListener, ItemListener{
    private Button btnOk, btnClose, btnClear, btnDelete;
    private TextField txtTitle;
    private List history;
    
    public TitleFrame (String t){
        super (t);
        setLayout (null);
        
        txtTitle = new TextField ("Title");
        txtTitle.setBounds (30, 30, 150, 20);
        txtTitle.addActionListener (this);
        
        btnOk = new Button("Ok");
        btnOk.setBounds(30, 70,150, 30);
        btnOk.addActionListener (this);
        
        btnClose = new Button("Close");
        btnClose.setBounds(180, 70,150, 30);
        btnClose.addActionListener (this);
        
        btnClear = new Button ("Clear");
        btnClear.setBounds (320, 70, 150, 30);
        btnClear.addActionListener (this);
        
        btnDelete = new Button ("Delete");
        btnDelete.setBounds (480, 70, 150, 30);
        btnDelete.addActionListener (this);
        
        history = new List ();
        history.setBounds (30, 100, 150, 100);
        history.addItemListener (this);
        
        add (btnOk);
        add (btnClose);
        add (btnClear);
        add (btnDelete);
        add (txtTitle);
        add (history);
    }
    
    private void setTextTitle(){
        this.setTitle(txtTitle.getText());
        history.add (txtTitle.getText());
    }
    
    public void actionPerformed (ActionEvent e){
        if(e.getSource() == btnOk){
            this.setTextTitle();
        }else if (e.getSource() == btnClose){
            System.exit(0);
        }else if (e.getSource() == btnClear){
            txtTitle.setText(" ");
            txtTitle.requestFocus();
        }else if (e.getSource() == btnDelete){
            if (history.getSelectedIndex() != -1){
                history.remove (history.getSelectedItem());
            }
        }else if (e.getSource() == txtTitle){
            this.setTextTitle();
        }
    }
    
    public void itemStateChanged(ItemEvent e){
         txtTitle.setText(history.getSelectedItem());
    } 
}
