package Lesson11;
import java.awt.*;
import java.awt.event.*;

public class LayoutDemo2 extends Frame{
    private Label lblName, lblSurname;
    private TextField txtName, txtSurname;
    private Button btnOk, btnCancel;
    private Panel pnlData, pnlButtons;
    
    public LayoutDemo2 (String title){
        super(title);
        
        pnlData = new Panel();
        pnlData.setLayout (new GridLayout(2,2));
        
        lblName = new Label ("Name: ");
        lblSurname = new Label ("Surname: ");
        txtName = new TextField();
        txtSurname = new TextField();
        
        pnlData.add(lblName);
        pnlData.add(txtName);
        pnlData.add(lblSurname);
        pnlData.add(txtSurname);
        
        add (pnlData, BorderLayout.CENTER);
        
        pnlButtons = new Panel();
        pnlButtons.setLayout(new FlowLayout (FlowLayout.CENTER));
        
        btnOk = new Button ("OK");
        btnCancel = new Button ("Cancel");
        pnlButtons.add(btnOk);
        pnlButtons.add(btnCancel);
        

        
        add (pnlButtons, BorderLayout.SOUTH);
        
        addWindowListener(new WindowAdapter (){
            public void windowClosing (WindowEvent w){
                System.exit(0);
            }
        });
    }
}
