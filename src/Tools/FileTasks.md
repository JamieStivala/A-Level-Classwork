# File Tasks
To-Do
- [x] Get File Name
- [x] Check File Availability
- [x] Get Amount Of Lines
- [x] Read Line
- [x] Add World
- [x] Get Random Word
- [x] Get File
- [x] Load to File


Dropped Methods
- Load String To File
    - loadToFile
- Load File To String
    - Replaced with getFile
- Read File
    - Replaced with Read Line
