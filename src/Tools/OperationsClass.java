package Tools;
import java.util.Scanner;
/**
 * This class will do basic operations
 *
 * rangeCheck --> Returns true if in range else false (Static Boolean) - Overwritten for an array of numbers
 * enterIntNumbers --> Asks the user to input an amount of numbers
 * enterDoubleNumbers --> Asks the user to input an amount of numbers
 * enterString --> Asks the user to input a String for an amount of times
 */
public class OperationsClass {

    //-------------------------------------------Range Check-------------------------------------------
    public static boolean rangeCheck(double number, double minRange, double maxRange){
        //Is number in range of minRange, maxRange
        return (minRange <= number && maxRange >= number);
    }

    public static boolean[] rangeCheck (double number[], double minRange, double maxRange){

        boolean ifTrue[] = new boolean[number.length];
        for (int i = 0; i != number.length;  i++){
            ifTrue[i] = minRange <= number[i] && maxRange >= number[i];
        }

        return ifTrue;
    }
    //-------------------------------------------Range Check-------------------------------------------



    //-------------------------------------------Enter Numbers (Int)-------------------------------------------
    public static int[] enterIntNumbers(int amount){
        Scanner scanner = new Scanner(System.in);
        int numbers[] = new int[amount];
        for (int i = 0; i != amount; i++){
            numbers[i] = scanner.nextInt();
        }
        return numbers;
    }

    public static int[] enterIntNumbers(int amount, String prompt){
        Scanner scanner = new Scanner(System.in);
        int numbers[] = new int[amount];
        for (int i = 0; i != amount; i++){
            System.out.println(prompt);
            numbers[i] = scanner.nextInt();
        }
        return numbers;
    }
    //-------------------------------------------Enter Numbers (Int)-------------------------------------------



    //-------------------------------------------Enter Numbers (Double)-------------------------------------------
    public static double[] enterDoubleNumbers(int amount){
        Scanner scanner = new Scanner(System.in);

        double numbers[] = new double[amount];
        for (int i = 0; i != amount; i++){
            numbers[i] = scanner.nextDouble();
        }
        return numbers;
    }

    public static double[] enterDoubleNumbers(int amount, String prompt){
        Scanner scanner = new Scanner(System.in);
        double numbers[] = new double[amount];
        for (int i = 0; i != amount; i++){
            System.out.println(prompt);
            numbers[i] = scanner.nextDouble();
        }
        return numbers;
    }
    //-------------------------------------------Enter Number (Double)-------------------------------------------


    //-------------------------------------------Enter String (String)-------------------------------------------
    public static String[] enterString(int amount){
        Scanner scanner = new Scanner(System.in);

        String numbers[] = new String[amount];
        for (int i = 0; i != amount; i++){
            numbers[i] = scanner.nextLine();
        }
        return numbers;
    }

    public static String[] enterStringNumbers(int amount, String prompt){
        Scanner scanner = new Scanner(System.in);
        String numbers[] = new String[amount];
        for (int i = 0; i != amount; i++){
            System.out.println(prompt);
            numbers[i] = scanner.nextLine();
        }
        return numbers;
    }
    //-------------------------------------------Enter String (String)-------------------------------------------
}
