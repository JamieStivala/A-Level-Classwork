package Tools;

/**
 * An OOP implementation of Digits Converter
 *
 * convert --> Converts number digits to text
 * convertCaps --> Convert number digits to text (With capital start)
 */
public class DigitsConverter {

    //-------------------------------------------Custom Names-------------------------------------------
    private static final String [] specialNames = {
            "",
            " thousand",
            " million",
            " billion",
            " trillion",
            " quadrillion",
            " quintillion"
    };

    private static final String[] tensNames = {
            "",
            " ten",
            " twenty",
            " thirty",
            " forty",
            " fifty",
            " sixty",
            " seventy",
            " eighty",
            " ninety"
    };

    private static final String[] numNames = {
            "",
            " one",
            " two",
            " three",
            " four",
            " five",
            " six",
            " seven",
            " eight",
            " nine",
            " ten",
            " eleven",
            " twelve",
            " thirteen",
            " fourteen",
            " fifteen",
            " sixteen",
            " seventeen",
            " eighteen",
            " nineteen"
    };
    //-------------------------------------------Custom Names-------------------------------------------


    //-------------------------------------------Conversions-------------------------------------------
    private static String convertLessThanOneThousand(int number){
        String outputNumber;

        if (number % 100 < 20){
            outputNumber = numNames[number % 100];
            number /= 100;
        }
        else {
            outputNumber = numNames[number % 10];
            number /= 10;

            outputNumber = tensNames[number % 10] + outputNumber;
            number /= 10;
        }
        if (number == 0) {return outputNumber;}
        return numNames[number] + " hundred" + outputNumber;
    }

    public static String convert(int number) {

        if (number == 0) {
            return "zero";
        }

        String prefix = "";

        if (number < 0) {
            number = -number;
            prefix = "negative";
        }

        String current = "";
        int place = 0;

        do {

            int n = number % 1000;

            if (n != 0){
                String s = convertLessThanOneThousand(n);
                current = s + specialNames[place] + current;
            }

            place++;
            number /= 1000;
        } while (number > 0);

        return (prefix + current).trim();
    }

    public static String convertCaps(int number){
        String numberS = convert(number);

        char numberC[] = numberS.toCharArray();
        numberC[0] = Character.toUpperCase(numberC[0]);

        numberS = String.copyValueOf(numberC);

        return numberS;

    }
    //-------------------------------------------Conversions-------------------------------------------

}