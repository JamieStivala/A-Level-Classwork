package Tools;

import java.io.*;
import java.util.ArrayList;

/**
 * This class will be used for anything that has to do with Files
 */


public class FileTasks {

    private String fileName;
    private BufferedReader bufferedReader;
    private ArrayList<String> file;

    public FileTasks(String fileName, boolean writing){
        this.fileName = fileName;
        file = new ArrayList<String>();
        if (!writing) {
            try {
                bufferedReader = new BufferedReader(new FileReader(fileName));
                readFile();
            } catch (IOException e) {
                System.err.println("Couldn't Start reader/Writer");
            }
        }
    }



    private void readFile(){
        String currentLine = null;
        try {
            while ((currentLine = bufferedReader.readLine()) != null) {
                this.file.add(currentLine);
            }
        }catch (IOException e){
            System.err.println("Couldn't read file");
        }
    }

    public String getFileName(){
        return this.fileName;
    }

    public ArrayList<String> getFile(){
        return this.file;
    }

    public static boolean checkFileAvailability(String fileName){
        return new File(fileName).exists();
    }

    public int getAmountOfLines(){
        return this.file.size();
    }

    public String readLine(int line){
        return this.file.get(line);
    }

    public void addLine(String line){
        this.file.add(line);
    }

    public String getRandomWord(){
        int random = (int)(Math.random() * (getAmountOfLines()));
        return this.file.get(random);
    }

    public void loadToFile(){
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.fileName))){
            for (String line : this.file) {
                bufferedWriter.write(line + "\n");
            }

        }catch (IOException e){
            System.err.println("Couldn't create file: " + e.getMessage());
        }
    }

    public void close(){
        try {
            bufferedReader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}