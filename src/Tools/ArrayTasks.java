package Tools;

/**
 * Tasks That Have To Do With Arrays
 * Final
 */
public class ArrayTasks {

    public static void outputArray(char arrayName[], boolean inOneLine, boolean spaceBetween) {
        if (inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i] + " ");
            }
        } else if (inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i]);
            }
        } else if (!inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i] + " ");
            }
        } else if (!inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i]);
            }
        }
    }


    public static void outputArray(int arrayName[], boolean inOneLine, boolean spaceBetween) {
        if (inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i] + " ");
            }
        } else if (inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i]);
            }
        } else if (!inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i] + " ");
            }
        } else if (!inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i]);
            }
        }
    }

    public static void outputArray(double arrayName[], boolean inOneLine, boolean spaceBetween) {
        if (inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i] + " ");
            }
        } else if (inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i]);
            }
        } else if (!inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i] + " ");
            }
        } else if (!inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i]);
            }
        }
    }

    public static void outputArray(String arrayName[], boolean inOneLine, boolean spaceBetween) {
        if (inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i] + " ");
            }
        } else if (inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.print(arrayName[i]);
            }
        } else if (!inOneLine && spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i] + " ");
            }
        } else if (!inOneLine && !spaceBetween) {
            for (int i = 0; i != arrayName.length; i++) {
                System.out.println(arrayName[i]);
            }
        }
    }


    public static char[] fillArray(char arrayName[], char character) {
        for (int i = 0; i != arrayName.length; i++) {
            arrayName[i] = character;
        }
        return arrayName;
    }

    public static int[] fillArray(int arrayName[], int number) {
        for (int i = 0; i != arrayName.length; i++) {
            arrayName[i] = number;
        }
        return arrayName;
    }

    public static double[] fillArray(double arrayName[], double number) {
        for (int i = 0; i != arrayName.length; i++) {
            arrayName[i] = number;
        }
        return arrayName;
    }

    public static String[] fillArray(String arrayName[], String word) {
        for (int i = 0; i != arrayName.length; i++) {
            arrayName[i] = word;
        }
        return arrayName;
    }



    public static void outputOneArrayPosition(char arrayName[], int position) {
        System.out.println(arrayName[position]);
    }

    public static void outputOneArrayPosition(int arrayName[], int position) {
        System.out.println(arrayName[position]);
    }

    public static void outputOneArrayPosition(double arrayName[], int position) {
        System.out.println(arrayName[position]);
    }

    public static void outputOneArrayPosition(String arrayName[], int position) {
        System.out.println(arrayName[position]);
    }



    public static void outputArrayForAmountOfTime(int number, int amount) {
        for (int i = 0; i != amount; i++) {
            System.out.println(number);
        }
    }

    public static void outputArrayForAmountOfTime(double number, int amount) {
        for (int i = 0; i != amount; i++) {
            System.out.println(number);
        }
    }
    public static void outputArrayForAmountOfTime(String word, int amount) {
        for (int i = 0; i != amount; i++) {
            System.out.println(word);
        }
    }



    public static int searchArray (String array[], String array2[]){
        int position = -1;
        for (int i = 0; i != array.length; i++){
            for (int x = 0; x != array2.length; x++){
                if (array[x].equalsIgnoreCase(array2[i])){
                    position = x;
                }
            }
        }
        return position;
    }

    public static int searchArray (String array[], String word){
        int position = -1;
        for (int i = 0; i != array.length; i++){
            if (array[i].equalsIgnoreCase(word)){
                position = i;
            }
        }
        return position;
    }
}
