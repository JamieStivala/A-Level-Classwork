package Tools;

/**
 * This class will be used for anything that has to do with the processing of numbers
 *
 * getAverage --> Gets the average of numbers[]
 * getFactors --> Gets the factors of number
 * getHCF --> Gets the factors of (number1, number2)
 * getLCM --> Gets the LCM of (number1, number2)
 * getRandomNumber --> Gets a random number of (range1, range2)
 */
public class NumberTasks {

    //-------------------------------------------Average-------------------------------------------
    public static double getAverage (int numbers[]){
        double average = 0;
        for (int i = 0; i != numbers.length; i++){
            average += numbers[i];
        }

        return average/numbers.length;

    }


    public static double getAverage (double numbers[]){
        double average = 0;
        for (int i = 0; i != numbers.length; i++){
            average += numbers[i];
        }

        return average/numbers.length;

    }
    //-------------------------------------------Average-------------------------------------------


    //-------------------------------------------Get Factors-------------------------------------------
    public static int[] getFactors (int number){
        int HCFStore[] = new int[(number)];
        int location = 0;
        for (int i = 1; i <= number; i++){
            if (number % i == 0){
                HCFStore[location] = i;
                location++;
            }
        }
        int Factors[] = new int[location];
        for (int i = 0; HCFStore[i] != 0; i++){
            Factors [i] = HCFStore[i];
        }
        return Factors;
    }
    //-------------------------------------------Get Factors-------------------------------------------



    //-------------------------------------------Get HCF-------------------------------------------
    private static long getHCF(long number1, long number2) {
        while (number2 > 0) {
            long temp = number2;
            number2 = number1 % number2;
            number1 = temp;
        }
        return number1;
    }

    public static long getHCF(long numbers[]) {

        long result = numbers[0];
        for(int i = 1; i < numbers.length; i++) result = getHCF(result, numbers[i]);
        return result;
    }
    //-------------------------------------------Get HCF-------------------------------------------



    //-------------------------------------------Get LCM-------------------------------------------
    private static long getLCM(long number1, long number2) {
        return number1 * (number2 / getHCF(number1, number2));
    }

    public static long getLCM(long numbers[]) {
        long result = numbers[0];
        for(int i = 1; i < numbers.length; i++){
            result = getLCM(result, numbers[i]);
        }
        return result;
    }
    //-------------------------------------------Get LCM-------------------------------------------


    //-------------------------------------------Get Random Number-------------------------------------------
    public static int getRandomNumber (int range1 , int range2){

        int bigNumber = Math.max(range1, range2);
        int smallNumber = Math.min(range1, range2);

        return smallNumber + (int)(Math.random() * (bigNumber - smallNumber));
    }
    //-------------------------------------------Get Random Number-------------------------------------------

}
