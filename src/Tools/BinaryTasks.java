/**
 * Notes to be taken --> Read as String to avoid: 01 --> 1; Used in signed conversion
 * If Number Is Incorrect --> Throws error NumberFormatException();
 */

package Tools;

public class BinaryTasks {

    public static int unsignedBinaryToDigits(String binaryNumber){
        char binaryNumArray[] = binaryNumber.toCharArray();

        int number = 0;
        boolean invalid = false;
        for (int loopStop = binaryNumArray.length - 1, counter = 0; loopStop != - 1; loopStop--, counter++) {
            if (binaryNumArray[loopStop] == '0'){
            }else if (binaryNumArray[loopStop] == '1'){
                number += (int)Math.pow(2, counter);
            }else{
                invalid = true;
            }
        }

        if(invalid){
            throw new NumberFormatException();
        }else{
            return number;
        }
    }

    public static int singedAndMagnitudeBinaryToDigits(String binaryNumber) {

        //Needed Vars
        char binaryNumArray[] = binaryNumber.toCharArray(); //The Number In Chars
        boolean isNegative = false;
        boolean invalid = false;
        int number = 0;

        if (binaryNumArray[0] == '1') {
            isNegative = true;
            binaryNumArray[0] = '0'; //So that it isn't added
        }else if (binaryNumArray[0] == '0'){
            isNegative = false;
        }else{
            invalid = true;
        }

        for (int loopStop = binaryNumArray.length - 1, counter = 0; loopStop != - 1; loopStop--, counter++){
            if(binaryNumArray[loopStop] == '0') {
            }else if (binaryNumArray[loopStop] == '1') {
                number += (int) Math.pow(2, counter);
            } else {
                invalid = true;
            }
        }

        if (invalid) {
            throw new NumberFormatException();
        } else {
            if (isNegative) {
                return number * -1;
            }else{
                return number;
            }
        }
    }


}
