package Lesson6.Classwork;

import java.util.Scanner;

public class BubbleSort{
    public static void main (String args[]){
        int a[] = {7, 23, 43, 65, 9, 2};
        Scanner scanner = new Scanner (System.in);
        
        for (int x = 1; x < a.length; x++){
            for (int i = 0; i < a.length - 1; i++){
                if (a [i] > a[i + 1]){
                    int swap = a [i];
                    a[i] = a [i + 1];
                    a [i + 1] = swap;
                }
            }
        }
        
        for (int i = 0; i != a.length; i++){
            System.out.println (a[i]);
        }
        
    }
}