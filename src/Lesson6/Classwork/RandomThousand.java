package Lesson6.Classwork;

public class RandomThousand{
    public static void main (String args[]){
        int a[] = new int [1000];
        int count = 0;
        for (int i =0; i != a.length; i++){
            a[i] = (int) (Math.random() * 1000) + 1;
            if (a[i] % 2 == 0){
                count++;
            }
        }
        
        System.out.println("There are " + count + " amount of numbers");
    }
}
