package Lesson6.Homework;

import java.util.Scanner;

public class isSorted {
    public static void main (String args[]) {
        boolean isSorted = true;
        Scanner scanner = new Scanner(System.in);
        int a[] = new int [10];

        for (int i = 0; i != a.length; i++){
            System.out.println("Enter number: ");
            a[i] = scanner.nextInt();
        }


        for (int x = 1; x < a.length; x++){
            for (int i = 0; i < a.length - 1; i++){
                if (a [i] > a[i + 1]){
                    isSorted = false;
                    int swap = a [i];
                    a[i] = a [i + 1];
                    a [i + 1] = swap;
                }
            }
        }

        for (int i = 0; i != a.length; i++){
            System.out.println (a[i]);
        }
        System.out.println("Array originally sorted: " + isSorted);
    }
}
