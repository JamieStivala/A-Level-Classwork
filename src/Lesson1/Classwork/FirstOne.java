package Lesson1.Classwork;

import java.util.Scanner;
/*
 * why public ?
 * in a file there can be many classes in a file but only one class can be public and i must be the one with the file's name
 */
public class FirstOne {
    /*
     * access specifier : shows the access of an object
     * keyword : A word the compiler recognises
     * identifier : A name of something (variable / class)
     * 
     * all classes start the same way 
     * AS K I
     */
    public static void main(String args []){
        /*
         * java 1.8 onwards String ... X is a way to initialise an array 
         * variable are stored in ram and must be given both a type and a name (identifier)
         * There are two types simple and complex
         * simple/ native : come with language (int, float)
         * complex: objects created by the user (String)
         * 
         * Simple Types :           8    16    32  64    bit numbers
         *               Integers : byte short int long : All of these are 2s compliment (all signed)
         *               Real : float single precision around 7 numbers after point
         *                      double double precision around 14 numbers after point
         *               char : 16 bit unsigned, uses UNICODE : includes latin and non-latin characters
         *               
         * Java is a strongly-typed language : This forces the programmer to  give the variable a type
         */
        
        String name;
        int age;
        /*
         * String is a complex type, hence it is a class in the standard packages from the jdk
         * String name; : A variable declaration
         * name = ""; : A variable initialisation
         * default value : The value given in initialisation
         */
        
        Scanner keyboard = new Scanner(System.in);
        //keyboard.useDelimiter("\n");
        /*
         * Scanner is a class just like String. But String does not need the new keyword to initialise, bt since Strings are so popular the compiler recognises Strings, but just Strings
         * System.in is the opposite of System.out, it gives the scanner individual tokens.
         * The scanner then uses methods like nextInt()/nextFloat to save the inputted information as the specified type
         */
        
        System.out.println("Enter name");
        name = keyboard.nextLine();
        
        System.out.println("Enter age");
        age = keyboard.nextInt();
        
        System.out.println(name + age);
        
        
                 
    }
}