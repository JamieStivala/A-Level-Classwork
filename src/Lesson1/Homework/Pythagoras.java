/*
Ask the user to input the length of 2 sides of a triangle and calculate and output the hyp of the triangle
 */

package Lesson1.Homework;
import java.util.Scanner;
public class Pythagoras {
    public static void main (String args []){
        Scanner scanner = new Scanner(System.in);

        //Inputs
        System.out.print("Please enter the length of side 1: ");
        float side1 = (float) Math.pow (scanner.nextFloat(), 2);
        System.out.print("\nPlease enter the length of side 2: ");
        float side2 = (float) Math.pow  (scanner.nextFloat(), 2);
        //Inputs

        /*
        hyp^2 = SQRT (a^2 + b^2);
         */

        System.out.print ("\n\nThe hypotenuse of the triangle is " + Math.sqrt(side1 + side2)); //Instantly output the result
    }
}
