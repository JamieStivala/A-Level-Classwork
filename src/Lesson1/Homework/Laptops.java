/*
Laptop A costs 350.99 Euros
Laptop B costs 300.99 Euros
Laptop C costs 900.99 Euros
Laptop D costs 1299.99 Euros

Ask the user to input how many laptop to order from each type and output the sub total for each laptop together with the grand total.
A VAT of 18% has to be applied on the total.  Output the VAT and the total including VAT
 */

package Lesson1.Homework;

import java.util.Scanner;

public class Laptops {
    public static void main (String args []){

        //Constants
        final float VAT = 0.18f;
        final float laptopPrices[] = {350.99f, 300.99f, 900.99f, 1299.99f};
        //Constants
        float subtotal [] = new float[laptopPrices.length];
        float total = 0;
        float vatTotal;
        //Menu
        String menu = (
                        "Laptop 1 costs: €" + laptopPrices [0] +
                        "\nLaptop 2 costs: €" + laptopPrices [1] +
                        "\nLaptop 3 costs: €" + laptopPrices [2] +
                        "\nLaptop 4 costs: €" + laptopPrices [3]);
        //Menu


        Scanner scanner = new Scanner(System.in);

        System.out.println("Products: \n" + menu);

        for (int i = 0; i != subtotal.length; i++){
            System.out.print("\nPlease enter the quantity of Laptop " + (i + 1) + ": ");
            subtotal [i] = laptopPrices [i] * scanner.nextFloat();
        }

        for (int i = 0; i != subtotal.length; i++){
            System.out.println("Subtotal of Laptop " + (i + 1) + " is €" + subtotal[i]);
            total += subtotal [i];
             if ( i == subtotal.length - 1){
                 System.out.println("Test total €" +total); //Test code
                 vatTotal = VAT * total;
                 System.out.println("With VAT of " + VAT + " the price increase is €" + vatTotal);
                 System.out.println("The total price is €" + (vatTotal + total));
             }
        }
    }
}
