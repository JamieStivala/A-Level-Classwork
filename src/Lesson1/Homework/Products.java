/*
 * Ask the user to input the name price and quantity for three items of stock.  The program should output the sub total for each item and the
 grand total.
 */

package Lesson1.Homework;
import java.util.Scanner;
public class Products {
    public static void main (String args []){
        //Imports
        Scanner stringReader = new Scanner(System.in);
        stringReader.useDelimiter ("\n");

        Scanner decimalReader = new Scanner(System.in);
        decimalReader.useDelimiter("\n");
        //Imports

        //Array creation
        final int AMOUNT_OF_PRODUCTS = 3;
        String productName [] = new String[AMOUNT_OF_PRODUCTS];
        float productSubTotal[] = new float[AMOUNT_OF_PRODUCTS];
        //Array creation

        //Inputs
        for (int i = 0; i != AMOUNT_OF_PRODUCTS; i++){
            System.out.print("\n\nPlease enter the product name of product number " + (i + 1) + ": ");
            productName[i] = stringReader.nextLine();

            System.out.print("\nPlease enter the price of the product: ");
            float priceOfProduct = decimalReader.nextFloat();

            System.out.print("\nPlease enter the quantity of the specified product: ");
            int quantity = decimalReader.nextInt();

            productSubTotal[i] = priceOfProduct * quantity;
        }
        //Inputs


        //Outputs
        for (int i = 0, total = 0; i != AMOUNT_OF_PRODUCTS; i++){
            System.out.println("\n\n\nThe subtotal of product " + productName [i] + " is " + productSubTotal [i]);
            total += productSubTotal [i];
            if (i == AMOUNT_OF_PRODUCTS - 1){
                System.out.println("\nAnd the total is " + total);
            }
        }
        //Outputs
    }
}
