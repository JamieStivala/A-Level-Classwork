# Problem 1
Class Name: Laptops
* Laptop A costs 350.99 Euros
* Laptop B costs 300.99 Euros
* Laptop C costs 900.99 Euros
* Laptop D costs 1299.99 Euros

Ask the user to input how many laptop to order from each type and output the sub total for each laptop together with the grand total.
A VAT of 18% has to be applied on the total.  Output the VAT and the total including VAT.

# Problem 2
Class name: Products

Ask the user to input the name price and quantity for three items of stock.  The program should output the sub total for each item and the
grand total.  

# Problem 3
Class name: Pythagoras

Ask the user to input the length of 2 sides of a triangle and calculate and output the hyp of the triangle.