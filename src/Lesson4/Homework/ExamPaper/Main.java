package Lesson4.Homework.ExamPaper;

import java.util.Scanner;

public class Main {

    public static void main (String args []){
        Scanner strings = new Scanner(System.in);
        Scanner number = new Scanner(System.in);

        //How many students and average
        System.out.print("Enter how many students you have: ");
        int students = number.nextInt();
        System.out.println();

        System.out.print("Enter the average mark: ");
        float averagePassMark = number.nextFloat();
        System.out.println();
        //How many students and average

        Exam exam[] = new Exam[students];
        Student student[] = new Student[exam.length];

        float mark;

        for (int i = 0; i != student.length; i++){
            //Init every instance
            student[i] = new Student();
            exam[i] = new Exam();
            exam[i].setAverage(averagePassMark);
            //Init every instance

            System.out.print("Please enter name of the student: ");
            student[i].name = strings.nextLine();
            System.out.println();

            System.out.print("Please enter surname of the student: ");
            student[i].surname = strings.nextLine();
            System.out.println();

            //Enter marks of the student
            do {
                System.out.print("Please enter mark of paper 1 of the student: ");
                mark = number.nextFloat();
                if (!Validation.isMarkValid(mark)){
                    System.out.println("Mark invalid please try again");
                }
            }while (!Validation.isMarkValid(mark));
            exam[i].setMark1(mark);
            System.out.println();

            do {
                System.out.print("Please enter mark of paper 2 of the student: ");
                mark = number.nextFloat();
                if (!Validation.isMarkValid(mark)){
                    System.out.println("Mark invalid please try again");
                }
            }while (!Validation.isMarkValid(mark));
            exam[i].setMark2(mark);
            System.out.println();

            do {
                System.out.print("Please enter mark of paper 3 of the student: ");
                mark = number.nextFloat();
                if (!Validation.isMarkValid(mark)){
                    System.out.println("Mark invalid please try again");
                }
            }while (!Validation.isMarkValid(mark));
            exam[i].setMark3(mark);
            System.out.println();
            //Enter marks of the student
        }

        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n");
        for (int i = 0; i != student.length; i++){
            System.out.printf("Student %s got an average of %.2f and therefor he ", student[i].getFullName(), exam[i].getAverage());
            if (exam[i].hasPassed()){
                System.out.println("passed");
            }else{
                System.out.println("didn't pass");
            }
        }


    }
}