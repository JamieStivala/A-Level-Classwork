package Lesson4.Homework.ExamPaper;

class Exam {
    private Paper paper[] = new Paper[3];
    private float average;

    void setAverage(float average){
        this.average = average;
    }
    void setMark1 (float mark){
        paper[0] = new Paper();
        paper[0].setMark(mark);
    }

    void setMark2 (float mark) {
        paper[1] = new Paper();
        paper[1].setMark(mark);
    }

    void setMark3 (float mark){
        paper[2] = new Paper();
        paper[2].setMark(mark);
    }

    float getAverage(){
        return (paper[0].getMark() + paper[1].getMark() + paper[2].getMark())/3;
    }

    boolean hasPassed(){
        return getAverage() >= average;
    }

}
