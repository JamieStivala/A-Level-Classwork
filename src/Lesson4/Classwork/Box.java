package Lesson4.Classwork;

public class Box{
    float width, height; //Instance Variables
    
    float getArea (){
        return width * height;
    }
    
    float getPerimiter(){
        return 2 * (width+height);
    }
    
    float getVolume (float lenght){
        return getArea() * lenght;
    }
    
    boolean isSquare (){
        return width == height;
    }
    
    //Overload
    void resizeBox (float commonFactor){
        width += commonFactor;
        height += commonFactor;
    }
    
    void resizeBox (float resizeWidth, float resizeHeight){
        width += resizeWidth;
        height += resizeHeight;
    }
    //Overload
    
    void newSize (float width, float height){
        this.width = width;
        this.height = height;
    }
}
