package Lesson12;

import java.io.*;

public class Client implements Serializable{
    private String name, surname, email;
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setSurname(String surname){
        this.surname = surname;
    }
    
    public void setEmail (String email){
        this.email = email;
    }
    
    public String getName(){
       return this.name;
    }
    
    public String getSurname(){
       return this.surname;
    }
    
    public String getEmail(){
       return this.email;
    }
}
