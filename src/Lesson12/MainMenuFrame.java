package Lesson12;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;
import java.io.*;

public class MainMenuFrame extends Frame implements ActionListener, WindowListener{
    private Button btnLoadClients;
    private Button btnSaveClients;
    private Button btnAddClient;
    private Button btnDeleteClient;
    private Button btnResetFile;
    private Button btnExit;
    private ArrayList <Client> clients;


    private AddClientFrame frmAddClient = null;
    private DeleteClientFrame frmDeleteClient = null;

    MainMenuFrame(String title, int w, int h){
        super(title);
        this.setSize(w, h);
        this.setLayout (null);

        btnLoadClients = new Button ("Load Clients");
        btnLoadClients.setBounds (130, 80, 100, 20);
        btnLoadClients.addActionListener(this);

        btnSaveClients = new Button ("Save Clients");
        btnSaveClients.setBounds (130, 120, 100, 20);
        btnSaveClients.addActionListener(this);

        btnAddClient = new Button ("Add Clients");
        btnAddClient.setBounds (130, 160, 100, 20);
        btnAddClient.addActionListener(this);

        btnDeleteClient = new Button ("Delete Clients");
        btnDeleteClient.setBounds (130, 200, 100, 20);
        btnDeleteClient.addActionListener(this);

        btnResetFile = new Button("Reset Clients");
        btnResetFile.setBounds(130, 240, 100, 20);
        btnResetFile.addActionListener(this);

        btnExit = new Button ("Exit");
        btnExit.setBounds (130, 280, 100, 20);
        btnExit.addActionListener (this);

        add(btnLoadClients);
        add(btnSaveClients);
        add(btnAddClient);
        add(btnDeleteClient);
        add(btnResetFile);
        add(btnExit);

        this.addWindowListener(this);
    }

    private boolean checkAvailability(){
        return new File(".clients.bin").exists();
    }

    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource() == btnAddClient){
            if(frmAddClient == null){
                frmAddClient = new AddClientFrame ("Add Client", 360, 400, clients);
            }
            frmAddClient.setVisible (true);
        }else if (e.getSource() == btnDeleteClient){
            if(frmDeleteClient == null){
                frmDeleteClient = new DeleteClientFrame ("Delete Client", 360, 400, clients);
            }
            frmDeleteClient.setVisible (true);
        }else if (e.getSource() == btnSaveClients){
            saveFile(false);
        }else if (e.getSource() == btnLoadClients){
            loadFile(false);
        }else if (e.getSource() == btnResetFile){
            resetFile();
        }else if (e.getSource() == btnExit){
            autoSave();
        }
    }

    private void resetFile(){
        if(JOptionPane.showConfirmDialog(null, "Are you sure you want to reset the file?", "Reset", JOptionPane.YES_NO_OPTION) == 0){
            clients = new ArrayList<>();
        }
    }

    private void saveFile(boolean autoSaved){
        try{
            ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream (".clients.bin"));
            f.writeObject(clients);

            if(!autoSaved) {
                JOptionPane.showMessageDialog(null, "File Saved", "Saving",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        }catch (IOException e){
            System.err.println ("Unable to save clients");
            JOptionPane.showMessageDialog (null, "File Not Saved - " + e.getMessage(), "Saving",
                JOptionPane.ERROR_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")
    private void loadFile(boolean autoLoaded){
        try{
            ObjectInputStream f = new ObjectInputStream (new FileInputStream (".clients.bin"));
            clients = (ArrayList <Client>) f.readObject();
            f.close();

            if(!autoLoaded) {
                JOptionPane.showMessageDialog(null, "File Loaded", "Loading",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog (null, "File Not Loaded - " + e.getMessage(), "Loading",
                JOptionPane.ERROR_MESSAGE);
        }
    }

    private void autoSave(){
        saveFile(true);
        System.exit(0);
    }

    @Override
    public void windowOpened(WindowEvent e) {
        if(!checkAvailability()) {
            clients = new ArrayList<>();
        }else{
            loadFile(true);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        autoSave();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
