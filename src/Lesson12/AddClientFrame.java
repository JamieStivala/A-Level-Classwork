package Lesson12;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class AddClientFrame extends Frame implements ActionListener, WindowListener{
    private Button btnAdd;
    private Button btnCancel;
    private TextField txtName;
    private TextField txtSurname;
    private TextField txtEmail;
    private Label lblName;
    private Label lblSurname;
    private Label lblEmail;

    private ArrayList <Client> clients;

    AddClientFrame(String title, int w, int h, ArrayList <Client> c) {
        super(title);
        this.setSize(w, h);
        this.setLayout(null);
        this.clients = c;

        btnAdd = new Button ("Add");
        btnAdd.setBounds (10, 200, 100, 20);
        btnAdd.addActionListener (this);

        btnCancel = new Button ("Cancel");
        btnCancel.setBounds (110, 200, 100, 20);
        btnCancel.addActionListener (this);

        lblName = new Label("Name");
        lblName.setBounds (10, 80, 100, 20);
        txtName = new TextField();
        txtName.setBounds (110, 80, 100, 20);

        lblSurname = new Label("Surname");
        lblSurname.setBounds (10, 120, 100, 20);
        txtSurname = new TextField();
        txtSurname.setBounds (110, 120, 100, 20);

        lblEmail = new Label("Email");
        lblEmail.setBounds (10, 160, 100, 20);
        txtEmail = new TextField();
        txtEmail.setBounds (110, 160, 100, 20);

        add(btnAdd);
        add(btnCancel);
        add(lblName);
        add(txtName);
        add(lblSurname);
        add(txtSurname);
        add(lblEmail);
        add(txtEmail);

        this.addWindowListener(this);
    }

    @Override
    public void actionPerformed (ActionEvent e){
        if (e.getSource() == btnAdd){
            addClient();
            clearFields();
        }

        this.setVisible (false);
    }
    
    private void clearFields(){
        txtName.setText ("");
        txtSurname.setText ("");
        txtEmail.setText ("");
    }

    private void addClient(){
        Client c = new Client();
        c.setName (txtName.getText());
        c.setSurname (txtSurname.getText());
        c.setEmail (txtEmail.getText());

        clients.add(c);
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.setVisible(false);
        this.clearFields();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
