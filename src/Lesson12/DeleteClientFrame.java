package Lesson12;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class DeleteClientFrame extends Frame implements ActionListener, WindowListener{
    private List lstClients;
    private Button btnDelete;
    private Button btnClose;
    private ArrayList <Client> clients;

    DeleteClientFrame(String title, int w, int h, ArrayList<Client> clients) {
        super(title);
        this.setSize(w, h);
        this.setLayout(null);
        this.clients = clients;

        lstClients = new List();
        lstClients.setBounds (10, 80, 100, 100);
        addToList();

        btnDelete = new Button("Delete");
        btnDelete.setBounds (130, 80, 100, 20);
        btnDelete.addActionListener(this);

        btnClose = new Button("Close");
        btnClose.setBounds (130, 160, 100, 20);
        btnClose.addActionListener (this);

        add(lstClients);
        add(btnClose);
        add(btnDelete);

        this.addWindowListener(this);
    }
    
    private void addToList(){
        for (Client c : clients){
            lstClients.add (c.getName() + " " + c.getSurname());
        }
    }
    
    public void actionPerformed (ActionEvent e){
        if(e.getSource() == btnDelete) {
            int currentIndex = lstClients.getSelectedIndex();
            lstClients.remove(currentIndex);
            clients.remove(currentIndex);
        }
        this.setVisible (false);
    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.setVisible(false);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
