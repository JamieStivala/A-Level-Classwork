/*
Implement a simple calculator that allows the user to input two numbers and displays a menu of 4 basic arithmetic operations.
The user can choose one operation and the program outputs the result for that operation.
 */
package Lesson2.Homework;
import java.util.Scanner;

public class Calculator {
    public static void main (String args[]){
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter how many times you'd like to enter numbers: ");
        int amount = scanner.nextInt();

        System.out.print(
                "\nIf you wanna use + enter 1" +
                "\nIf you wanna use - enter 2" +
                "\nIf you wanna use * enter 3" +
                "\nIf you wanna use / enter 4");
        int sign = scanner.nextInt();
        float total = 0;
        for (int i = 0; i != amount; i++){
            System.out.println();
            System.out.print("Please enter number: " + i);
            float number = scanner.nextFloat();
            switch (sign){
                case 1:
                    total += number;
                    break;
                case 2:
                    total -= number;
                    break;
                case 3:
                    total = 1;
                    total *= number;
                    break;
                case 4:
                    total = 1;
                    total /= number;
                    break;
                default:
                    System.out.println("\nThe value entered as a sign is invalid. \nHave fun with entering " + amount + " for nothing.");
            }

        }
        System.out.println("\nThe final total is " + total);
    }
}
