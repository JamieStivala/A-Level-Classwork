# Payroll
A company pays its employees by the hour.

The normal hourly rate (up to 40hr) is 8.75/h.

The hourly rate for overtime hours is 10.15/h.

Write a java program that asks the user to input the name and the time worked. Output the total

# Calculator
Implement a simple calculator that allows the user to input two numbers and displays a menu of 4 basic arithmetic operations.

The user can choose one operation and the program outputs the result for that operation.

# Grades
Modify the program to include more grades (D,E) and also to output the message invalid mark if the mark inputted is not in the range
0..100

# Menu
Make a menu of any sort.