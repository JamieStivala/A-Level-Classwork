package Lesson2.Homework;
import java.util.Scanner;
/*
80 - 100
65 -- 79
55 -- 64
45 -- 54
35 -- 44
 */
public class Grades {
    public static void main (String args []) {
        Scanner scanner = new Scanner(System.in);
        float mark = 0;

        do{
            System.out.print("Please enter the mark: ");
            mark = scanner.nextFloat();
        }while(mark > 100 || mark < 0);

        System.out.println(); //Skips a line

        if (mark >= 80 && mark <= 100){
            System.out.println("A");
        }else if (mark >= 65 && mark <= 79){
            System.out.println("B");
        }else if (mark >= 55 && mark <= 64){
            System.out.println("C");
        }else if (mark >= 45 && mark <= 54){
            System.out.println("D");
        }else if (mark >= 35 && mark <= 44){
            System.out.println("E");
        }else{
            System.out.println("F");
        }
    }
}
