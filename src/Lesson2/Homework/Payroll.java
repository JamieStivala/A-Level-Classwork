/*
A company pays its employees by the hour.
The normal hourly rate (up to 40hr) is 8.75/h.
The hourly rate for overtime hours is 10.15/h.
Write a java program that asks the user to input the name and the time worked. Output the total
 */
package Lesson2.Homework;
import java.util.Scanner;

public class Payroll {
    public static void main (String args[]){
        //Importing the scanners
        Scanner string = new Scanner(System.in);
        Scanner numbers = new Scanner(System.in);
        //Importing the scanners

        //Constants
        final float HOUR_RATE = 8.75f;
        final float HOUR_RATE_OVERTIME  = 10.15f;
        //Constants

        //User inputs
        System.out.print("Enter the name of the worker: ");
        String name = string.nextLine();

        System.out.print("\nEnter the amount of hours worked: ");
        int hours = numbers.nextInt();
        //User inputs
        float total = 0;
        if (hours < 40){
            total = 40 * HOUR_RATE;
            total += (hours - 40) * HOUR_RATE_OVERTIME;
        }else {
            total = HOUR_RATE * hours;
        }

        System.out.printf("\nThe worker %s gets a total pay of %.2f", name, total);


    }
}
