package Lesson2.Classwork;

import java.util.Scanner;
public class Teenager{
    public static void main (String args[]){
        Scanner scanner = new Scanner (System.in);
        
        System.out.println("Please enter age");
        int age = scanner.nextInt();
        
        if (age > 19 || age < 13){
            System.out.println ("Not in range");
        }else{
            System.out.println ("In range");
        }
    }
}
