package Lesson2.Classwork;

import java.util.Scanner;

public class ExamPass{
    public static void main (String args[]){
        Scanner scanner = new Scanner (System.in);
        final int PASS_MARK = 50;
        
        System.out.println("Please enter student mark Paper 1");
        float paper1 = scanner.nextFloat();
        
        System.out.println ("Please enter student mark of Paper 2");
        float paper2 = scanner.nextFloat();
        
        if (paper1 >= PASS_MARK && paper2 >= PASS_MARK){
            System.out.println("You passed!");
        }else {
            System.out.println("You haven't passed");
        }
    }
}
