package Lesson2.Classwork;
/*
 * A computer technician chargers 15 euro an hour for his work plus the materials charge
 * for a minimum of 25 euro.  Input the client name and the number of hours, and the
 * materials charge.  Output the total cost making it 25 euros where needed.
 */
import java.util.Scanner;

public class Technician {
    public static void main (String args[]){
        final float HOUR_RATE = 15f;
        Scanner numbers = new Scanner (System.in);
        Scanner string = new Scanner (System.in);
        
        System.out.print("Please enter the name of the client: ");
        String clientName = string.nextLine();
        
        System.out.print("\nPlease enter the amount of time worked: ");
        float time = numbers.nextFloat();
        
        System.out.println ("\nPlease enter the materials charge: ");
        float materialCharge = numbers.nextFloat();
        
        float total = (time * HOUR_RATE) + materialCharge;
        if (total < 25){
            total = 25;
        }
        System.out.println (clientName + " " + total); 
    }
    
}
