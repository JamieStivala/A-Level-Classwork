package Lesson2.Classwork;

import java.util.Scanner;
public class RandomGame{
    public static void main (String args[]){
        Scanner scanner = new Scanner (System.in);
        int random = (int)(Math.random() * 20) + 1;
        
        //System.out.println(random); //Test line
        
        System.out.println ("Please enter number of range 1-20");
        int number = scanner.nextInt();
        if (number == random){
            System.out.println("You won");
        }else{
            System.out.println("You Lost");
        }
    }
}
