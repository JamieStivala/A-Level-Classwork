package Lesson2.Classwork;

import java.util.Scanner;
public class MarkingScheme{
    public static void main (String args[]){
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Please enter the mark");
        float mark = scanner.nextFloat();
        
        if (mark >= 80 && mark <= 100){
            System.out.println("A");
        }else if (mark >= 65 && mark <= 79){
            System.out.println("B");
        }else if (mark >= 55 && mark <= 64){
            System.out.println("C");
        }else {
            System.out.println("F");
        }
    }
}
