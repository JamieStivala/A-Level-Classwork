package Lesson9.Classwork;

import Lesson7.Classwork.Employee;

import java.io.*;
import java.util.*;

public class LoadingEmployee{
    public static void main (String args[]){
        try{
            ArrayList <Employee> employee;
            ObjectInputStream f = new ObjectInputStream (new FileInputStream ("employee.bin"));
            employee = (ArrayList<Employee>)f.readObject();
            f.close();
            
            for (Employee e : employee){
                System.out.println(e.getName());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
