package Lesson9.Classwork;

import java.io.*;
import java.util.*;

public class ScannerInput{
    public static void main (String args []){
        try {
            Scanner scan = new Scanner (new FileReader ("test.txt"));
            int tot = 0, num;
            while(scan.hasNext()){
                num = scan.nextInt();
                System.out.print(num);
                tot += num;
            }
            System.out.println(tot);
        }catch (IOException e){
            e.printStackTrace();
        }
        
    }
}