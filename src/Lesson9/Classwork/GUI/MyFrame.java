package Lesson9.Classwork.GUI;

import java.awt.*;
import java.awt.event.*;
public class MyFrame extends Frame{
    private Button btnOK;
    private Button btnCancel;
    public MyFrame(String title, int w, int h){
        super(title);
        this.setSize(w, h);
        this.setLayout (null);
        
        addWindowListener (new WindowAdapter (){
            public void windowClosing (WindowEvent we){ 
                System.exit(0);
            }
        });
        
        btnOK = new Button ("OK");
        btnOK.setBounds (100, 380, 100, 20);
        
        btnCancel = new Button ("CANCEL");
        btnCancel.setBounds (200, 380, 100, 20);
        add(btnOK);
        add(btnCancel);
    }
    
    public MyFrame(String title){
        super(title);
        
        addWindowListener (new WindowAdapter (){
            public void windowClosing (WindowEvent we){ 
                System.exit(0);
            }
        });
    }
    
    public MyFrame(){
        addWindowListener (new WindowAdapter (){
            public void windowClosing (WindowEvent we){ 
                System.exit(0);
            }
        });
    }
    
    @Override
    public void paint(Graphics g){
        g.setColor (Color.RED);
        g.fillRect(50, 50, 150, 200);
        g.drawOval (30, 30, 200, 100);
        g.setColor (Color.BLACK);
        g.drawRoundRect (100,100, 50, 50, 5, 5);
        g.drawString ("Stwing Ma Boi", 678, 78);
    }
}
