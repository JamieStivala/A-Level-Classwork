package Lesson9.Classwork;
import Lesson7.Classwork.Employee;
import Lesson7.Classwork.Manager;
import Lesson7.Classwork.Salesman;

import java.util.*;
import java.io.*;

public class SavingEmployees{
    public static void main (String args[]){
        try{
            ArrayList <Employee> emp = new ArrayList <>();
            emp.add (new Manager("0368201L", "Martin", 12000));
            emp.add (new Manager ("0654234M", "JamSti", 21000));
            emp.add (new Salesman("343429M", "Aidun", 5.6f, 9200));
            emp.add (new Salesman ("243465M", "Screw", 9.8f, 8700));
            
            ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream ("employee.bin"));
            f.writeObject(emp);
            f.close();
            System.out.println("Data Saved");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
