package Lesson8.Classwork;

import java.io.*;

/**
 * Write a description of class CountWords here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class CountWords{
    public static void main (String args[]){
        try{
            BufferedReader x = new BufferedReader (new FileReader ("file.txt"));
            
            String line;
            int amount = 0;
            while ((line = x.readLine()) != null){
                
                amount += line.split (" ").length; 
            }
            
            System.out.println (amount);
            
            x.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
