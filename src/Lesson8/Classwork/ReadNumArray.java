package Lesson8.Classwork;

import java.io.*;
/**
 * Write a description of class LoadFile here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ReadNumArray{
    public static void main (String args[]){
        try{
            BufferedReader file = new BufferedReader (new FileReader("file.txt"));
            String line;
            int tot = 0;
            int x[] = new int [10];
            for(int i = 0; i != x.length; i++){
                line = file.readLine();
                x[i] = Integer.parseInt(line);
                
                tot += x[i];
            }
            
            System.out.println (tot/10);
            

            /**
            String x = null;
            int y = 0;
            while ((x = file.readLine()) != null){
                System.out.println(y);
            }
            */
            
            file.close();
            //Read will return -1 if its EOF --> Else will return the file in int
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
