package Lesson8.Classwork;

import java.io.*;
import java.util.*;
/**
 * Write a description of class LoadFile here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ReadNumList{
    public static void main (String args[]){
        try{
            BufferedReader file = new BufferedReader (new FileReader("file.txt"));
            int tot = 0;
            ArrayList<Integer> nums = new ArrayList<Integer>();

            
            String line = null;
            int y = 0;
            while ((line = file.readLine()) != null){
                nums.add(Integer.parseInt(line));
                tot += nums.get(y);
                y++;
            }
            
            int avg = tot / nums.size();
            for (int x : nums){
                if (x >= avg) {
                    System.out.println(x);
                }
            }
            System.out.println (tot/nums.size());
            file.close();
            //Read will return -1 if its EOF --> Else will return the file in int
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
