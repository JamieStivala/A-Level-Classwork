package Lesson8.Classwork;


import java.util.*;
import java.io.*;
public class LoadExamMarks{
    public static void main (String args []){
        try {
            BufferedReader x = new BufferedReader (new FileReader ("file.txt"));
            ArrayList <ExamMark> mark = new ArrayList <ExamMark>();
            
            ExamMark tempMarkO;
            String line;
            
            while ((line = x.readLine()) != null){        
                tempMarkO = new ExamMark ();
                tempMarkO.name = line;
                tempMarkO.mark = Integer.parseInt(x.readLine());
                
                mark.add(tempMarkO);
            }
            int tot = 0;
            for (ExamMark e : mark){
                tot += e.mark;
            } 
            
            //Ask the user to input the name of the student
            //Display his/her mark
            
            Scanner scanner = new Scanner (System.in);
            
            System.out.println ("Enter the name of the student ");
            String name = scanner.nextLine();
            
            boolean found = false;
            
            for (int i = 0; i != mark.size(); i++){
                ExamMark tempMark = mark.get (i);
                if (tempMark.name.equalsIgnoreCase (name)){
                    System.out.println (tempMark.mark);
                    found = true;
                }
            }
            
            if (!found){
                System.out.println ("Can't find user");
            }
            
            //Ask the user to input a mark and list all users those students who got greater or equal to the inputted mark
            
            System.out.println ("Enter the mark ");
            int userInput = scanner.nextInt();
            
            for (ExamMark e : mark){
                if (e.mark >= userInput){
                    System.out.println (e.name);
                }
            }
            
            //Saving all the data back
            FileWriter w = new FileWriter ("file.txt");
            for (ExamMark e : mark){
                w.write(e.name + "\r\n");
                w.write(e.mark + "\r\n");
            }
            
            w.close();
            
        }catch(IOException e){e.printStackTrace();}
    }
}
