package Lesson8.Classwork;

import Tools.FileTasks;

import java.util.ArrayList;

/**
 * Write a description of class LoadFile here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class LoadFile{
    public static void main (String args[]) {
        FileTasks file = new FileTasks("file.txt", true);
        ArrayList<String> f =  file.getFile();
        file.addLine("Jamie");
        file.addLine("Is");
        file.addLine("IDK man");
        file.loadToFile();
        file.close();
    }
}
