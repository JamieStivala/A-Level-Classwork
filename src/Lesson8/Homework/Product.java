package Lesson8.Homework;

public class Product {
    private String name;
    private float price;

    public void setPrice(float price){
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
