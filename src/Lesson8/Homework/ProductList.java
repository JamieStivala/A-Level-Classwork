package Lesson8.Homework;

import Tools.FileTasks;

import java.util.ArrayList;
import java.util.Scanner;

public class ProductList {
    public static void main (String args[]){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter of store name: ");
        String store = scanner.nextLine();
        System.out.println("1. Add to file\n2. Read from file");
        int choice = scanner.nextInt();
        if(!FileTasks.checkFileAvailability(store + ".txt") || choice == 1) {
            FileTasks file = new FileTasks(store + ".txt", true);
            System.out.print("\nPlease enter how many products you would like to enter: ");
            int amount = scanner.nextInt();

            for(int i = 0; i != amount; i++){
                System.out.print("\n\nProduct Name: ");
                file.addLine(scanner.nextLine());
                System.out.print("\nPrice: ");
                file.addLine(scanner.nextInt() + "");
            }
            file.loadToFile();
        }else if(choice == 2){
            FileTasks file = new FileTasks(store + ".txt", false);
            //Load to Product
            ArrayList<Product> products = new ArrayList<Product>();
            for (int  i = 0; i != file.getAmountOfLines(); i++){
                Product p = new Product();
                p.setName(file.getFile().get(i));
                i++;
                p.setPrice(Integer.parseInt(file.getFile().get(i)));
                products.add(p);
            }
            scanner = new Scanner(System.in);
            System.out.print("Please enter the name of the product: ");
            String productName = scanner.nextLine();

            for (int i = 0; i != products.size(); i++){
                Product p = products.get(i);
                if (p.getName().equalsIgnoreCase(productName)){
                    System.out.println(p.getPrice());
                }
            }
        }

    }
}
