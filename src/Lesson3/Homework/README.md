# Prime
Ask the user to input an integer number and output a message stating weather n is a prime number or not.

# Fibonacci
Ask the user to input an integer number n.
Output the n'th term of the fibonacci sequence

# Calculator
Write a java program that shows the following menu

* 0 - Enter number
* 1 - Addition
* 2 - Minus
* 3 - Multiplication
* 4 - Division
* X - Exit

The program should repeatedly execute the menu until the user selects the exit option