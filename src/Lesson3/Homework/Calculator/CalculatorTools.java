package Lesson3.Homework.Calculator;

class CalculatorTools {
    static boolean signsCorrect(char currentSign) {
        return (currentSign == '1' || currentSign == '2' || currentSign == '3' || currentSign == '4');
    }

    static boolean enterOrExit(char currentSign){
        return (currentSign == 'X' || currentSign == 'x' ||  currentSign == '0');
    }

    static float doSum (char currentSign, float total, float number){
        if (currentSign == '3'){
            total *= number;
        }else if (currentSign == '4'){
            //So it doesn't crash
            if (total == 0){
                total = 1;
            }

            if (number == 0){
                number = 1;
            }
            //So it doesn't crash
            total /= number;
        }else if (currentSign == '1'){
            total += number;
        }else if (currentSign == '2'){
            total -= number;
        }
        return total;
    }

}
