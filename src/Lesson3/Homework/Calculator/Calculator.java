package Lesson3.Homework.Calculator;

import java.util.Scanner;
public class Calculator {
    public static void main (String args []){
        Scanner scanner = new Scanner(System.in);

        String enterOrExit =
                "0 - Enter Number\n" +
                "X - Exit";

        String signs =
                "1 - Addition\n" +
                "2 - Minus\n" +
                "3 - Multiplication\n" +
                "4 - Division\n";


        System.out.println(enterOrExit);
        char currentChar = scanner.next().charAt(0);
        float total = 0;

        //Initializing the number so the program doesn't crash/give a false value if the user does 0/5 or give - if he does 0*5
        if (currentChar != 'X' && currentChar != 'x') {
            System.out.print("\n\nPlease enter a number: ");
            float number = scanner.nextFloat();

            do {
                System.out.println("\n" + signs);
                currentChar = scanner.next().charAt(0);

                if (currentChar == '3') {
                    total = 1;
                    total *= number;
                } else if (currentChar == '4') {
                    total = 1;
                    total /= number;
                } else if (currentChar == '1') {
                    total += number;
                } else if (currentChar == '2') {
                    total -= number;
                }
            }while (!CalculatorTools.signsCorrect(currentChar));
        }
        //Initializing the number so the program doesn't crash if the user does 0/5 or give - if he does 0*5

        //Main Loop
        while (currentChar != 'X' && currentChar != 'x'){
            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            System.out.println ("Current total is: " + total);

            //Another number or exit
            System.out.println(enterOrExit);
            currentChar = scanner.next().charAt(0);
            while (!CalculatorTools.enterOrExit(currentChar)){
                System.out.println("Character Invalid Please Try Again");
                currentChar = scanner.next().charAt(0);
            }
            //Another number or exit

            //Work it out
            if (currentChar == '0'){
                System.out.print("\n\nPlease enter a number: ");
                float number = scanner.nextFloat();

                System.out.println("\n\n" + signs);
                currentChar = scanner.next().charAt(0);
                while (!CalculatorTools.signsCorrect(currentChar)){
                    System.out.println("Character incorrect please try again");
                    currentChar = scanner.next().charAt(0);
                }
                total = CalculatorTools.doSum(currentChar, total, number);
            }
            //Work it out


        }
        //Main Loop
    }
}
