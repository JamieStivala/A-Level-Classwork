package Lesson3.Homework;
import java.util.Scanner;
public class Prime {
    public static void main (String args []){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a number: ");
        int number = scanner.nextInt();

        int amountOfPrime = 0;
        for (int i = 1; i != number; i++){
            if (number % i == 0){
                amountOfPrime++;
            }
        }

        if (amountOfPrime == 1){
            System.out.println("\nThis number is a prime number");
        }else{
            System.out.println("\nThis number isn't a prime number");
        }
    }
}
