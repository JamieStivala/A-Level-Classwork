package Lesson3.Homework;
import java.util.Scanner;
public class Fibonacci {
    public static void main (String args []){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the number you want the n term of: ");
        int number = scanner.nextInt();

        int firstNumber = 0;
        int secondNumber = 1;
        int fibonacci = 0;

        for (int i = 0; i != number - 1; i++){

            /*
            Fibonacci: 0
            First: 0
            Second: 1

            Fibonacci: 1
            First: 1
            Second: 1

            Fibonacci: 2
            First: 1
            Second: 2

            Fibonacci: 3
            First: 2
            Second: 3

            --> Dry run for visualisation.
             */

            fibonacci = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = fibonacci;
        }

        System.out.println("\n\n The n'th term is: " + fibonacci);

    }
}
