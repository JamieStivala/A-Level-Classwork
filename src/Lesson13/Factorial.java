package Lesson13;

/**
 * Write a description of class Factorial here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Factorial{
    public static double iFactorial(int x){
        long fact = 1;
        
        for(int i = 2; i <= x; i++){
            fact *= i;
        }
        
        return fact;
    }
    
    public static double rFactorial(int x){
        if(x == 0){
            return 1;
        }else{
            return x * rFactorial(x-1);
        } 
    }
    
    public static double rSummersion(int x){
        if(x == 0){
            return 0;
        }else{
            return x + rSummersion(x-1);
        } 
    }
    
    public static double rPow(int x, int y){
        if (y == 0){
            return 1;
        }else if (x == 0){
            return 0;
        }else{
            return x * rPow (x, y - 1);
        }
    }
    
    public static long rRec(int n){
        if (n == 0 || n == 1){
            return n;
        }else{
            return rRec (n -1) + rRec (n-2);
        }
    }
    
    public static void main (String args[]){
        System.out.println(rFactorial (4));
    }
}
