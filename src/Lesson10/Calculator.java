package Lesson10;

import java.awt.*;
import java.awt.event.*;

public class Calculator extends Frame{
    private Label lblNum1, lblNum2, lblAns;
    private TextField txtNum1, txtNum2;
    private Button btnAdd, btnSub, btnExit;
    
    public Calculator(String title){
        super(title);
        setLayout(null);
        
        //Creating the button
        lblNum1 = new Label ("Num1");
        lblNum2 = new Label ("Num2");
        lblAns = new Label ("Answer");
        
        txtNum1 = new TextField ("0");
        txtNum2 = new TextField ("0");
        
        btnAdd = new Button ("Add");
        btnSub = new Button ("Sub");
        btnExit = new Button ("Exit");
        //Creating the button
        
        //Setting the bounds
        lblNum1.setBounds (20, 20, 40, 20);
        lblNum2.setBounds (20, 50, 40, 20);
        lblAns.setBounds (80, 70, 80, 40);
        
        txtNum1.setBounds (60, 20, 40, 20);
        txtNum2.setBounds (60, 50, 40, 20);
        
        btnAdd.setBounds(20, 120, 40, 40);
        btnSub.setBounds(60, 120, 40, 40);
        btnExit.setBounds(100, 120, 40, 40);
        
        add(lblNum1);
        add(lblNum2);
        add(lblAns);
        
        add(txtNum1);
        add(txtNum2);
        
        add(btnAdd);
        add(btnSub);
        add(btnExit);
        
        
        addWindowListener (new WindowAdapter (){
            public void windowClosing (WindowEvent we){ 
                System.exit(0);
            }
        });
    }
}
